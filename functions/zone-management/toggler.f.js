const admin = require('firebase-admin')
const functions = require('firebase-functions')
const axios = require('axios')
const dayjs = require('dayjs')
const _ = require('lodash')
const config = require('./config')
const formatResponse = require('../../helpers/format-response')
const Log = require('../../helpers/log')
const getConfigMap = require('./zones/get-config-map')
const getZonesToProcess = require('./zones/get-zones-to-process')
const getZonesWithoutDuplicates = require('./zones/get-zones-without-duplicates')
const getZonesWithExpiringEffect = require('./zones/get-zones-with-expiring-effect')
const { logToLogistics } = require('./logging')

const db = admin.firestore()
const log = new Log(config.slack.development, 'Zone management - Toggler')

function wrapPromise(promise) {
  return promise
    .then(result => ({ success: true, result }))
    .catch(error => ({ success: false, error }))
}

// We need to limit requests to API to 1 concurrent request at time
const AXIOS_MAX_REQUESTS_COUNT = 1
const AXIOS_THROTTLE_CHECK_INTERVAL = 10
let axiosPendingRequests = 0

axios.interceptors.request.use((axiosConfig) => new Promise((resolve) => {
  const interval = setInterval(() => {
    if (axiosPendingRequests < AXIOS_MAX_REQUESTS_COUNT) {
      axiosPendingRequests += 1
      clearInterval(interval)
      resolve(axiosConfig)
    }
  }, AXIOS_THROTTLE_CHECK_INTERVAL)
}))

axios.interceptors.response.use((response) => {
  axiosPendingRequests = Math.max(0, axiosPendingRequests - 1)
  return Promise.resolve(response)
}, (error) => {
  axiosPendingRequests = Math.max(0, axiosPendingRequests - 1)
  return Promise.reject(error)
})

module.exports = functions
  .region('europe-west1')
  .https
  .onRequest(async (request, response) => {
    // current time with timezone
    const now = new Date()

    // select collection
    const configRef = db.collection(config.firestore.zonesConfig)
    const zonesRef = db.collection(config.firestore.zones)

    // get current state of restaurant groups
    const groupStateResponse = await axios.get(`${config.internal.url}/api/v3/restaurant-group-state`, {
      headers: config.internal.headers,
    })

    const zonesWithOngoingCloseEffect = groupStateResponse.data.data.filter(item => item.cooldown !== null)
    const zonesWithOngoingDdtEffect = groupStateResponse.data.data.filter(item => item.deliveryTimeChange !== null)

    // get config
    configRef.get()
      .then((configCollection) => {
        const configMap = getConfigMap(configCollection.docs)
        const configIdMap = configCollection.docs.map(item => ({
          id: item.id,
          id_internal: item.data().id_internal,
        }))

        // get all documents
        zonesRef.get()
          .then((zonesCollection) => {
            const filteredDocs = getZonesToProcess(zonesCollection.docs, configMap, now, config.firestore.documentMaxAge, zonesWithOngoingCloseEffect, zonesWithOngoingDdtEffect)
            const filteredDocsWithoutDuplicates = getZonesWithoutDuplicates(filteredDocs)
            const serverRequests = []
            const handledGroupIds = {
              close: [],
              ddt1: [],
              ddt2: [],
              deactivateDdt: [],
              deactivateClose: [],
            }

            // prepare DEACTIVATE DDT
            if (filteredDocsWithoutDuplicates.deactivateDdt) {
              filteredDocsWithoutDuplicates.deactivateDdt.forEach((item) => {
                const serverRequest = axios.delete(`${config.internal.url}/api/v3/restaurant-groups-delivery-time/${item}`, { headers: config.internal.headers })
                serverRequests.push(serverRequest)
                handledGroupIds.deactivateDdt.push(item)
              })
            }

            // prepare DEACTIVATE CLOSE
            if (filteredDocsWithoutDuplicates.deactivateClose) {
              filteredDocsWithoutDuplicates.deactivateClose.forEach((item) => {
                const serverRequest = axios.put(`${config.internal.url}/api/v3/restaurant-groups/${item}`, {
                  onCooldown: false,
                }, { headers: config.internal.headers })
                serverRequests.push(serverRequest)
                handledGroupIds.deactivateClose.push(item)
              })
            }

            // prepare CLOSE
            if (filteredDocsWithoutDuplicates.close) {
              const zones = getZonesWithExpiringEffect({
                zones: filteredDocsWithoutDuplicates.close,
                zonesWithOngoingEffect: zonesWithOngoingCloseEffect,
                timestampPath: 'cooldown.till',
                now,
                effectLookahead: config.time.effectLookahead,
              })
              zones.forEach((item) => {
                const serverRequest = axios.put(`${config.internal.url}/api/v3/restaurant-groups/${item}`, {
                  onCooldown: true,
                  till: dayjs(now).add(config.time.close.validity, 'minute').format(),
                }, { headers: config.internal.headers })
                serverRequests.push(serverRequest)
                handledGroupIds.close.push(item)
              })
            }

            // prepare DDT2
            if (filteredDocsWithoutDuplicates.ddt2) {
              const zones = getZonesWithExpiringEffect({
                zones: filteredDocsWithoutDuplicates.ddt2,
                zonesWithOngoingEffect: zonesWithOngoingDdtEffect,
                timestampPath: 'deliveryTimeChange.deliveryTimeValidity',
                now,
                effectLookahead: config.time.effectLookahead,
              })
              zones.forEach((item) => {
                const serverRequest = axios.put(`${config.internal.url}/api/v3/restaurant-groups-delivery-time/${item}`, {
                  deliveryTimeModifier: config.time.ddt2.modifier,
                  deliveryTimeModifierValidity: dayjs(now).add(config.time.ddt2.validity, 'minute').format(),
                }, { headers: config.internal.headers })
                serverRequests.push(serverRequest)
                handledGroupIds.ddt2.push(item)
              })
            }

            // prepare DDT1
            if (filteredDocsWithoutDuplicates.ddt1) {
              const zones = getZonesWithExpiringEffect({
                zones: filteredDocsWithoutDuplicates.ddt1,
                zonesWithOngoingEffect: zonesWithOngoingDdtEffect,
                timestampPath: 'deliveryTimeChange.deliveryTimeValidity',
                now,
                effectLookahead: config.time.effectLookahead,
              })
              zones.forEach((item) => {
                const serverRequest = axios.put(`${config.internal.url}/api/v3/restaurant-groups-delivery-time/${item}`, {
                  deliveryTimeModifier: config.time.ddt1.modifier,
                  deliveryTimeModifierValidity: dayjs(now).add(config.time.ddt1.validity, 'minute').format(),
                }, { headers: config.internal.headers })
                serverRequests.push(serverRequest)
                handledGroupIds.ddt1.push(item)
              })
            }

            // handle all requests
            Promise.all(serverRequests.map(wrapPromise))
              .then((result) => {
                const failedRequests = result.filter(item => item.success === false)
                  .map(item => _.get(item, 'error.request.path'))
                const info = {
                  now: dayjs(now).format(),
                  IDs: handledGroupIds,
                  failedRequests,
                }

                logToLogistics({
                  webhook: config.slack.logistics,
                  handledGroupIds,
                  configIdMap,
                  now: dayjs(now).format(),
                })

                if (failedRequests.length) {
                  log.error(info)
                  response.status(400).send(formatResponse(400, info))
                } else {
                  console.info(info)
                  response.status(200).send(formatResponse(200, info))
                }
              }).catch((error) => {
                log.error(error)
                response.status(400).send(formatResponse(400, error))
              })
          })
          .catch((error) => {
            log.error(error)
            response.status(400).send(formatResponse(400, error))
          })
      })
      .catch((error) => {
        log.error(error)
        response.status(400).send(formatResponse(400, error))
      })
  })
