const getZoneGroups = require('./get-zone-groups')

describe('GetZoneGroups tests', () => {
  it('Successfully gets zone groups - single prop matched', () => {
    const docs = [
      {
        id: 3,
        data: {
          delay: 15,
        },
      },
    ]

    const configMap = {
      3: [
        {
          ddt1: 2,
          ddt2: 4,
          close: 15,
          id_hurrier: 3,
          id_internal: 333,
        },
      ],
    }

    const result = getZoneGroups(docs, configMap)

    expect(result).toEqual({
      close: [
        333,
      ],
    })
  })

  it('Successfully gets zone groups - all possible props matched', () => {
    const docs = [
      {
        id: 2,
        data: {
          delay: 10,
        },
      },
      {
        id: 3,
        data: {
          delay: 20,
        },
      },
      {
        id: 4,
        data: {
          delay: 29,
        },
      },
      {
        id: 5,
        data: {
          delay: 15,
        },
      },
    ]

    const configMap = {
      2: [
        {
          ddt1: 2,
          ddt2: 4,
          close: 10,
          id_hurrier: 2,
          id_internal: 222,
        },
      ],
      3: [
        {
          ddt1: 20,
          ddt2: 25,
          close: 35,
          id_hurrier: 3,
          id_internal: 333,
        },
      ],
      4: [
        {
          ddt1: 10,
          ddt2: 17,
          close: 30,
          id_hurrier: 4,
          id_internal: 444,
        },
      ],
      5: [
        {
          ddt1: 10,
          ddt2: 12,
          close: 15,
          id_hurrier: 5,
          id_internal: 555,
        },
      ],
    }

    const result = getZoneGroups(docs, configMap)

    expect(result).toEqual({
      ddt1: [
        333,
      ],
      ddt2: [
        444,
      ],
      close: [
        222,
        555,
      ],
    })
  })

  it('Gets no zone groups - no matched property', () => {
    const docs = [
      {
        id: 1,
        data: {
          delay: 5,
        },
      },
    ]

    const configMap = {
      1: [
        {
          ddt1: 10,
          ddt2: 20,
          close: 25,
          id_hurrier: 1,
          id_internal: 111,
        },
      ],
    }

    const result = getZoneGroups(docs, configMap)

    expect(result).toEqual({})
  })

  it('Successfully gets zone groups - all possible props matched - 1:n', () => {
    const docs = [
      {
        id: 1,
        data: {
          delay: 20,
        },
      },
      {
        id: 2,
        data: {
          delay: 15,
        },
      },
    ]

    const configMap = {
      1: [
        {
          ddt1: 2,
          ddt2: 4,
          close: 10,
          id_hurrier: 1,
          id_internal: 222,
        },
        {
          ddt1: 20,
          ddt2: 25,
          close: 35,
          id_hurrier: 1,
          id_internal: 333,
        },
      ],
      2: [
        {
          ddt1: 10,
          ddt2: 12,
          close: 30,
          id_hurrier: 2,
          id_internal: 444,
        },
        {
          ddt1: 10,
          ddt2: 12,
          close: 15,
          id_hurrier: 2,
          id_internal: 555,
        },
      ],
    }

    const result = getZoneGroups(docs, configMap)

    expect(result).toEqual({
      ddt1: [
        333,
      ],
      ddt2: [
        444,
      ],
      close: [
        222,
        555,
      ],
    })
  })

  it('Successfully gets zone groups - all possible props matched - 1:n (with deactivate)', () => {
    const docs = [
      {
        id: 1,
        data: {
          delay: 20,
        },
      },
      {
        id: 2,
        data: {
          delay: 5,
        },
      },
      {
        id: 3,
        data: {
          delay: 25,
        },
      },
    ]

    const configMap = {
      1: [
        {
          ddt1: 2,
          ddt2: 4,
          close: 10,
          id_hurrier: 1,
          id_internal: 222,
        },
        {
          ddt1: 20,
          ddt2: 25,
          close: 35,
          id_hurrier: 1,
          id_internal: 333,
        },
      ],
      2: [
        {
          ddt1: 10,
          ddt2: 12,
          close: 30,
          id_hurrier: 2,
          id_internal: 444,
        },
        {
          ddt1: 10,
          ddt2: 12,
          close: 15,
          id_hurrier: 2,
          id_internal: 555,
        },
      ],
      3: [
        {
          ddt1: 10,
          ddt2: 12,
          close: 30,
          id_hurrier: 3,
          id_internal: 123,
        },
      ],
    }

    const ongoingClose = [
      {
        groupId: 123,
      },
    ]

    const ongoingDdt = [
      {
        groupId: 123,
      },
      {
        groupId: 444,
      },
    ]

    const result = getZoneGroups(docs, configMap, ongoingClose, ongoingDdt)

    expect(result).toEqual({
      ddt1: [
        333,
      ],
      close: [
        222,
      ],
      deactivateClose: [
        123,
      ],
      deactivateDdt: [
        444,
      ],
    })
  })
})
