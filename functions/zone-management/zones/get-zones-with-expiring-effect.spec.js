const getZonesWithExpiringEffect = require('./get-zones-with-expiring-effect')

describe('GetZonesWithExpiringEffect tests', () => {
  it('CLOSE - No zones with ongoing effect', () => {
    const now = new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)')
    const timestampPath = 'cooldown.till'
    const effectLookahead = 2

    const zones = [
      1,
      2,
    ]

    const zonesWithOngoingEffect = []

    const result = getZonesWithExpiringEffect({
      zones,
      zonesWithOngoingEffect,
      timestampPath,
      now,
      effectLookahead,
    })

    expect(result).toEqual([
      1,
      2,
    ])
  })

  it('CLOSE - One zone with ongoing effect', () => {
    const now = new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)')
    const timestampPath = 'cooldown.till'
    const effectLookahead = 2

    const zones = [
      1,
      2,
    ]

    const zonesWithOngoingEffect = [{
      groupId: 1,
      cooldown: {
        till: new Date('Wed Nov 21 2018 15:50:00 GMT+0100 (Central European Standard Time)'),
      },
      deliveryTimeChange: null,
    }]

    const result = getZonesWithExpiringEffect({
      zones,
      zonesWithOngoingEffect,
      timestampPath,
      now,
      effectLookahead,
    })

    expect(result).toEqual([
      2,
    ])
  })

  it('CLOSE - Multiple zones with ongoing effect', () => {
    const now = new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)')
    const timestampPath = 'cooldown.till'
    const effectLookahead = 2

    const zones = [
      1,
      2,
      3,
    ]

    const zonesWithOngoingEffect = [{
      groupId: 1,
      cooldown: {
        till: new Date('Wed Nov 21 2018 15:50:00 GMT+0100 (Central European Standard Time)'),
      },
      deliveryTimeChange: null,
    },
    {
      groupId: 2,
      cooldown: {
        till: new Date('Wed Nov 21 2018 15:50:00 GMT+0100 (Central European Standard Time)'),
      },
      deliveryTimeChange: null,
    },
    {
      groupId: 3,
      cooldown: {
        till: new Date('Wed Nov 21 2018 15:50:00 GMT+0100 (Central European Standard Time)'),
      },
      deliveryTimeChange: null,
    }]

    const result = getZonesWithExpiringEffect({
      zones,
      zonesWithOngoingEffect,
      timestampPath,
      now,
      effectLookahead,
    })

    expect(result).toEqual([])
  })

  it('CLOSE - Multiple zones with ongoing effect and multiple zones with expiring effect', () => {
    const now = new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)')
    const timestampPath = 'cooldown.till'
    const effectLookahead = 2

    const zones = [
      1,
      2,
      3,
      4,
      5,
    ]

    const zonesWithOngoingEffect = [{
      groupId: 1,
      cooldown: {
        till: new Date('Wed Nov 21 2018 15:50:00 GMT+0100 (Central European Standard Time)'),
      },
      deliveryTimeChange: null,
    },
    {
      groupId: 2,
      cooldown: {
        till: new Date('Wed Nov 21 2018 15:50:00 GMT+0100 (Central European Standard Time)'),
      },
      deliveryTimeChange: null,
    },
    {
      groupId: 4,
      cooldown: {
        till: new Date('Wed Nov 21 2018 15:31:00 GMT+0100 (Central European Standard Time)'),
      },
      deliveryTimeChange: null,
    },
    {
      groupId: 5,
      cooldown: {
        till: new Date('Wed Nov 21 2018 15:31:55 GMT+0100 (Central European Standard Time)'),
      },
      deliveryTimeChange: null,
    }]

    const result = getZonesWithExpiringEffect({
      zones,
      zonesWithOngoingEffect,
      timestampPath,
      now,
      effectLookahead,
    })

    expect(result).toEqual([
      3,
      4,
      5,
    ])
  })

  it('DDT - No zones with ongoing effect', () => {
    const now = new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)')
    const timestampPath = 'deliveryTimeChange.deliveryTimeValidity'
    const effectLookahead = 2

    const zones = [
      1,
      2,
    ]

    const zonesWithOngoingEffect = []

    const result = getZonesWithExpiringEffect({
      zones,
      zonesWithOngoingEffect,
      timestampPath,
      now,
      effectLookahead,
    })

    expect(result).toEqual([
      1,
      2,
    ])
  })

  it('DDT - One zone with ongoing effect', () => {
    const now = new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)')
    const timestampPath = 'deliveryTimeChange.deliveryTimeValidity'
    const effectLookahead = 2

    const zones = [
      1,
      2,
    ]

    const zonesWithOngoingEffect = [{
      groupId: 1,
      cooldown: null,
      deliveryTimeChange: {
        deliveryTimeValidity: new Date('Wed Nov 21 2018 15:50:00 GMT+0100 (Central European Standard Time)'),
      },
    }]

    const result = getZonesWithExpiringEffect({
      zones,
      zonesWithOngoingEffect,
      timestampPath,
      now,
      effectLookahead,
    })

    expect(result).toEqual([
      2,
    ])
  })

  it('DDT - Multiple zones with ongoing effect', () => {
    const now = new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)')
    const timestampPath = 'deliveryTimeChange.deliveryTimeValidity'
    const effectLookahead = 2

    const zones = [
      1,
      2,
      3,
    ]

    const zonesWithOngoingEffect = [{
      groupId: 1,
      cooldown: null,
      deliveryTimeChange: {
        deliveryTimeValidity: new Date('Wed Nov 21 2018 15:50:00 GMT+0100 (Central European Standard Time)'),
      },
    },
    {
      groupId: 2,
      cooldown: null,
      deliveryTimeChange: {
        deliveryTimeValidity: new Date('Wed Nov 21 2018 15:50:00 GMT+0100 (Central European Standard Time)'),
      },
    },
    {
      groupId: 3,
      cooldown: null,
      deliveryTimeChange: {
        deliveryTimeValidity: new Date('Wed Nov 21 2018 15:50:00 GMT+0100 (Central European Standard Time)'),
      },
    }]

    const result = getZonesWithExpiringEffect({
      zones,
      zonesWithOngoingEffect,
      timestampPath,
      now,
      effectLookahead,
    })

    expect(result).toEqual([])
  })

  it('DDT - Multiple zones with ongoing effect and multiple zones with expiring effect', () => {
    const now = new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)')
    const timestampPath = 'deliveryTimeChange.deliveryTimeValidity'
    const effectLookahead = 2

    const zones = [
      1,
      2,
      3,
      4,
      5,
    ]

    const zonesWithOngoingEffect = [{
      groupId: 1,
      cooldown: null,
      deliveryTimeChange: {
        deliveryTimeValidity: new Date('Wed Nov 21 2018 15:50:00 GMT+0100 (Central European Standard Time)'),
      },
    },
    {
      groupId: 2,
      cooldown: null,
      deliveryTimeChange: {
        deliveryTimeValidity: new Date('Wed Nov 21 2018 15:50:00 GMT+0100 (Central European Standard Time)'),
      },
    },
    {
      groupId: 4,
      cooldown: null,
      deliveryTimeChange: {
        deliveryTimeValidity: new Date('Wed Nov 21 2018 15:31:00 GMT+0100 (Central European Standard Time)'),
      },
    },
    {
      groupId: 5,
      cooldown: null,
      deliveryTimeChange: {
        deliveryTimeValidity: new Date('Wed Nov 21 2018 15:31:55 GMT+0100 (Central European Standard Time)'),
      },
    }]

    const result = getZonesWithExpiringEffect({
      zones,
      zonesWithOngoingEffect,
      timestampPath,
      now,
      effectLookahead,
    })

    expect(result).toEqual([
      3,
      4,
      5,
    ])
  })
})
