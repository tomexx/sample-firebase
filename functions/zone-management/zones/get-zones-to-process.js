const dayjs = require('dayjs')
const getZoneGroups = require('./get-zone-groups')

const hasValidTimestamp = (doc, now, maxAge) => {
  const timestamp = dayjs(doc.data.timestamp.toDate())
  return dayjs(now).diff(timestamp, 'minute') <= maxAge
}

// don't continue when old documents are found and process only documents from the config map
const filterDocs = (docs, configMap, now, maxAge) => docs.filter((doc) => (
  Object.prototype.hasOwnProperty.call(configMap, doc.id) && hasValidTimestamp(doc, now, maxAge)
))

module.exports = function getZonesToProcess(docs, configMap, now, maxAge, ongoingClose, ongoingDdt) {
  const mappedDocs = docs.map(doc => ({
    id: doc.id,
    data: doc.data(),
  }))

  // zones to use modifiers
  const filteredDocs = filterDocs(mappedDocs, configMap, now, maxAge)
  return getZoneGroups(filteredDocs, configMap, ongoingClose, ongoingDdt)
}
