const getZonesToProcess = require('./get-zones-to-process')

describe('GetZonesToProcess tests', () => {
  it('Returns zones to process based on provided docs and config map - all applicable records in config map', () => {
    const docs = [
      {
        id: 1,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
        otherProp: 444,
      },
      {
        id: 2,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
      },
      {
        id: 3,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
        otherProp: 444,
      },
      {
        id: 4,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
      },
    ]

    const configMap = {
      1: [
        {
          close: 5,
          id_internal: 111,
        },
      ],
      2: [
        {
          close: 5,
          id_internal: 222,
        },
      ],
      3: [
        {
          close: 50,
          id_internal: 333,
        },
      ],
      4: [
        {
          close: 50,
          ddt1: 30,
          ddt2: 40,
          id_internal: 444,
        },
      ],
    }

    const ongoingClose = [
      {
        groupId: 333,
      },
    ]

    const ongoingDdt = [
      {
        groupId: 444,
      },
    ]

    // 1 min later than timestamp in documents
    const now = new Date('Wed Nov 21 2018 15:31:00 GMT+0100 (Central European Standard Time)')

    // max age of document in minutes
    const maxAge = 2

    const result = getZonesToProcess(docs, configMap, now, maxAge, ongoingClose, ongoingDdt)

    expect(result).toEqual({
      close: [
        111,
        222,
      ],
      deactivateClose: [
        333,
      ],
      deactivateDdt: [
        444,
      ],
    })
  })

  it('Returns zones to process based on provided docs and config map - one delay value is negative', () => {
    const docs = [
      {
        id: 1,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
        otherProp: 444,
      },
      {
        id: 2,
        data: () => ({
          delay: -20,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
      },
    ]

    const configMap = {
      1: [
        {
          close: 5,
          id_internal: 111,
        },
      ],
      2: [
        {
          close: 5,
          id_internal: 222,
        },
      ],
    }

    // 1 min later than timestamp in documents
    const now = new Date('Wed Nov 21 2018 15:31:00 GMT+0100 (Central European Standard Time)')

    // max age of document in minutes
    const maxAge = 2

    const result = getZonesToProcess(docs, configMap, now, maxAge, [], [])

    expect(result).toEqual({
      close: [
        111,
      ],
    })
  })

  it('Returns zones to process based on provided docs and config map - multiple delay values are negative', () => {
    const docs = [
      {
        id: 1,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
      },
      {
        id: 2,
        data: () => ({
          delay: -20,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
      },
      {
        id: 3,
        data: () => ({
          delay: -20,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
      },
      {
        id: 4,
        data: () => ({
          delay: -20,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
      },
    ]

    const configMap = {
      1: [
        {
          close: 5,
          id_internal: 111,
        },
      ],
      2: [
        {
          close: 5,
          id_internal: 222,
        },
      ],
      3: [
        {
          close: 5,
          id_internal: 333,
        },
        {
          close: 5,
          id_internal: 555,
        },
      ],
      4: [
        {
          close: 5,
          id_internal: 444,
        },
        {
          ddt: 10,
          id_internal: 555,
        },
      ],
    }

    // 1 min later than timestamp in documents
    const now = new Date('Wed Nov 21 2018 15:31:00 GMT+0100 (Central European Standard Time)')

    // max age of document in minutes
    const maxAge = 2

    const result = getZonesToProcess(docs, configMap, now, maxAge, [], [])

    expect(result).toEqual({
      close: [
        111,
      ],
    })
  })

  it('Returns no zones to process based on provided docs and config map - invalid max. age of documents', () => {
    const docs = [
      {
        id: 1,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
        otherProp: 444,
      },
      {
        id: 2,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
      },
    ]

    const configMap = {
      1: [
        {
          close: 5,
          id_internal: 111,
        },
      ],
      2: [
        {
          close: 5,
          id_internal: 222,
        },
      ],
    }

    // 1 min later than timestamp in documents
    const now = new Date('Wed Nov 21 2018 15:33:00 GMT+0100 (Central European Standard Time)')

    // max age of document in minutes
    const maxAge = 2

    const result = getZonesToProcess(docs, configMap, now, maxAge, [], [])

    expect(result).toEqual({})
  })

  it('Returns zones to process based on provided docs and config map - one applicable record in config map', () => {
    const docs = [
      {
        id: 1,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
      },
      {
        id: 2,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
      },
    ]

    const configMap = {
      1: [
        {
          close: 5,
          id_internal: 111,
        },
      ],
    }

    // 1 min later than timestamp in documents
    const now = new Date('Wed Nov 21 2018 15:31:00 GMT+0100 (Central European Standard Time)')

    // max age of document in minutes
    const maxAge = 2

    const result = getZonesToProcess(docs, configMap, now, maxAge, [], [])

    expect(result).toEqual({
      close: [
        111,
      ],
    })
  })

  it('Returns no zones to process based on provided docs and config map - no applicable record in config map', () => {
    const docs = [
      {
        id: 1,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
        otherProp: 444,
      },
      {
        id: 2,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
      },
    ]

    const configMap = {
      1: [
        {
          close: 500,
          id_internal: 111,
        },
      ],
    }

    // 1 min later than timestamp in documents
    const now = new Date('Wed Nov 21 2018 15:31:00 GMT+0100 (Central European Standard Time)')

    // max age of document in minutes
    const maxAge = 2

    const result = getZonesToProcess(docs, configMap, now, maxAge, [], [])

    expect(result).toEqual({})
  })

  it('Returns no zones to process based on provided docs and config map - no item from config map matched', () => {
    const docs = [
      {
        id: 1,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
        otherProp: 444,
      },
      {
        id: 2,
        data: () => ({
          delay: 15,
          timestamp: {
            toDate: () => new Date('Wed Nov 21 2018 15:30:00 GMT+0100 (Central European Standard Time)'),
          },
        }),
      },
    ]

    const configMap = {
      999: [
        {
          close: 5,
          id_internal: 111,
        },
      ],
    }

    // 1 min later than timestamp in documents
    const now = new Date('Wed Nov 21 2018 15:31:00 GMT+0100 (Central European Standard Time)')

    // max age of document in minutes
    const maxAge = 2

    const result = getZonesToProcess(docs, configMap, now, maxAge, [], [])

    expect(result).toEqual({})
  })
})
