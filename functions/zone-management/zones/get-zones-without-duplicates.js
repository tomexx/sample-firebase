const _ = require('lodash')

module.exports = function getZonesWithoutDuplicates(docs) {
  const result = _.cloneDeep(docs)
  const buffer = new Set()

  _.forOwn(docs, (prop, key) => {
    prop.forEach((item) => {
      if (buffer.has(item)) {
        // remove item if it is in buffer already
        result[key].splice(result[key].indexOf(item), 1)
      } else {
        // add occourence to buffer
        buffer.add(item)
      }
    })
  })

  // return without empty props
  _.forOwn(result, (prop, key) => {
    if (!result[key].length) {
      delete result[key]
    }
  })

  return result
}
