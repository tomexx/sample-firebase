const getConfigMap = require('./get-config-map')

describe('GetConfigMap tests', () => {
  it('Successfully gets empty map from no docs', () => {
    const docs = []

    const result = getConfigMap(docs)

    expect(result).toEqual({})
  })

  it('Successfully gets config map from provided docs - 1:1', () => {
    const docs = [
      {
        id: 1,
        data: () => ({
          id_internal: 111,
          id_hurrier: 444,
          close: 10,
        }),
        otherProp: 444,
      },
      {
        id: 2,
        data: () => ({
          id_internal: 222,
          id_hurrier: 888,
          close: 10,
        }),
      },
    ]

    const result = getConfigMap(docs)

    expect(result).toEqual({
      444: [
        {
          id: 1,
          id_internal: 111,
          id_hurrier: 444,
          close: 10,
        },
      ],
      888: [
        {
          id: 2,
          id_internal: 222,
          id_hurrier: 888,
          close: 10,
        },
      ],
    })
  })

  it('Successfully gets config map from provided docs - 1:n', () => {
    const docs = [
      {
        id: 1,
        data: () => ({
          id_internal: 111,
          id_hurrier: 444,
          close: 10,
        }),
        otherProp: 444,
      },
      {
        id: 2,
        data: () => ({
          id_internal: 222,
          id_hurrier: 888,
          close: 10,
        }),
      },
      {
        id: 3,
        data: () => ({
          id_internal: 333,
          id_hurrier: 444,
          close: 10,
        }),
      },
    ]

    const result = getConfigMap(docs)

    expect(result).toEqual({
      444: [
        {
          id: 1,
          id_internal: 111,
          id_hurrier: 444,
          close: 10,
        },
        {
          id: 3,
          id_internal: 333,
          id_hurrier: 444,
          close: 10,
        },
      ],
      888: [
        {
          id: 2,
          id_internal: 222,
          id_hurrier: 888,
          close: 10,
        },
      ],
    })
  })
})
