const _ = require('lodash')
const dayjs = require('dayjs')

module.exports = function getZonesWithExpiringEffect({
  zones,
  zonesWithOngoingEffect,
  timestampPath,
  now,
  effectLookahead,
}) {
  const result = _.cloneDeep(zones)

  return result.filter((item) => {
    const match = _.find(zonesWithOngoingEffect, { groupId: item })
    if (match) {
      try {
        const timestamp = dayjs(_.get(match, timestampPath))
        return timestamp.diff(dayjs(now), 'minute') <= effectLookahead
      } catch (error) {
        console.error(error)
      }
    }
    return true
  })
}
