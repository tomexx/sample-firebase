const getZonesWithoutDuplicates = require('./get-zones-without-duplicates')

describe('GetZonesWithoutDuplicates tests', () => {
  it('No duplicates found', () => {
    const docs = {
      close: [
        1,
        2,
      ],
      ddt1: [
        3,
        4,
      ],
      ddt2: [
        5,
        6,
      ],
    }

    const result = getZonesWithoutDuplicates(docs)

    expect(result).toEqual({
      close: [
        1,
        2,
      ],
      ddt1: [
        3,
        4,
      ],
      ddt2: [
        5,
        6,
      ],
    })
  })

  it('Single duplicate in the same group', () => {
    const docs = {
      close: [
        1,
        1,
      ],
      ddt1: [
        3,
        4,
      ],
      ddt2: [
        5,
        6,
      ],
    }

    const result = getZonesWithoutDuplicates(docs)

    expect(result).toEqual({
      close: [
        1,
      ],
      ddt1: [
        3,
        4,
      ],
      ddt2: [
        5,
        6,
      ],
    })
  })

  it('Single duplicate in the same and in a different group', () => {
    const docs = {
      close: [
        1,
        1,
      ],
      ddt1: [
        1,
        4,
      ],
      ddt2: [
        5,
        6,
      ],
    }

    const result = getZonesWithoutDuplicates(docs)

    expect(result).toEqual({
      close: [
        1,
      ],
      ddt1: [
        4,
      ],
      ddt2: [
        5,
        6,
      ],
    })
  })

  it('Single duplicate in the same and in a different group (one of the groups remains empty)', () => {
    const docs = {
      close: [
        1,
        1,
      ],
      ddt1: [
        1,
      ],
      ddt2: [
        5,
        6,
      ],
    }

    const result = getZonesWithoutDuplicates(docs)

    expect(result).toEqual({
      close: [
        1,
      ],
      ddt2: [
        5,
        6,
      ],
    })
  })

  it('Two duplicates in two dfferent groups', () => {
    const docs = {
      close: [
        1,
        1,
        5,
      ],
      ddt1: [
        1,
      ],
      ddt2: [
        5,
        6,
      ],
    }

    const result = getZonesWithoutDuplicates(docs)

    expect(result).toEqual({
      close: [
        1,
        5,
      ],
      ddt2: [
        6,
      ],
    })
  })

  it('Two groups duplicated with duplicates inside groups', () => {
    const docs = {
      close: [
        1,
        2,
        3,
      ],
      ddt1: [
        1,
        1,
        2,
        2,
        3,
        3,
      ],
      ddt2: [
        1,
        1,
        2,
        2,
        3,
        3,
      ],
    }

    const result = getZonesWithoutDuplicates(docs)

    expect(result).toEqual({
      close: [
        1,
        2,
        3,
      ],
    })
  })
})
