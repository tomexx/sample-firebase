const _ = require('lodash')

module.exports = function getZoneGroups(docs, configMap, ongoingClose, ongoingDdt) {
  const result = {}
  _.forOwn(docs, (doc) => {
    configMap[doc.id].forEach((configMapItem) => {
      if (
        _.find(ongoingClose, { groupId: configMapItem.id_internal })
        && doc.data.delay < configMapItem.close
      ) {
        if (!Object.prototype.hasOwnProperty.call(result, 'deactivateClose')) {
          result.deactivateClose = []
        }
        result.deactivateClose.push(configMapItem.id_internal)
      } else if (
        _.find(ongoingDdt, { groupId: configMapItem.id_internal })
        && doc.data.delay < configMapItem.ddt2
        && doc.data.delay < configMapItem.ddt1
      ) {
        if (!Object.prototype.hasOwnProperty.call(result, 'deactivateDdt')) {
          result.deactivateDdt = []
        }
        result.deactivateDdt.push(configMapItem.id_internal)
      } else if (doc.data.delay >= configMapItem.close) {
        if (!Object.prototype.hasOwnProperty.call(result, 'close')) {
          result.close = []
        }
        result.close.push(configMapItem.id_internal)
      } else if (doc.data.delay >= configMapItem.ddt2) {
        if (!Object.prototype.hasOwnProperty.call(result, 'ddt2')) {
          result.ddt2 = []
        }
        result.ddt2.push(configMapItem.id_internal)
      } else if (doc.data.delay >= configMapItem.ddt1) {
        if (!Object.prototype.hasOwnProperty.call(result, 'ddt1')) {
          result.ddt1 = []
        }
        result.ddt1.push(configMapItem.id_internal)
      }
    })
  })
  return result
}
