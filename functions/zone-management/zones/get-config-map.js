module.exports = function getConfigMap(docs) {
  const result = {}
  docs.forEach((item) => {
    if (!Object.prototype.hasOwnProperty.call(result, item.data().id_hurrier)) {
      result[item.data().id_hurrier] = []
    }
    result[item.data().id_hurrier].push({ ...item.data(), id: item.id })
  })
  return result
}
