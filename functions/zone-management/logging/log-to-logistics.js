const axios = require('axios')
const _ = require('lodash')

const formatZones = ({ zones, configIdMap }) => zones.map((item) => {
  const result = _.find(configIdMap, ['id_internal', item])
  return `${result.id} (${result.id_internal})`
})

module.exports = function logToLogistics({
  webhook,
  handledGroupIds,
  configIdMap,
  now,
}) {
  const blocks = []
  if (handledGroupIds.close.length) {
    blocks.push({
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `*Efekt CLOSE: ${formatZones({ zones: handledGroupIds.close, configIdMap }).join(', ')}*\n`,
      },
    })
  }
  if (handledGroupIds.deactivateClose.length) {
    blocks.push({
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `*Zrušení efektu CLOSE: ${formatZones({ zones: handledGroupIds.deactivateClose, configIdMap }).join(', ')}*\n`,
      },
    })
  }
  if (blocks.length) {
    blocks.unshift({
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: 'Byla provedena úprava zón:\n',
      },
    })
    blocks.push({
      type: 'context',
      elements: [
        {
          type: 'mrkdwn',
          text: `Čas na servru: ${now}`,
        },
      ],
    })
    blocks.push({
      type: 'divider',
    })
    axios.put(webhook, { blocks })
  }
}
