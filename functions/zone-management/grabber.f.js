const admin = require('firebase-admin')
const functions = require('firebase-functions')
const axios = require('axios')
const config = require('./config')
const formatResponse = require('../../helpers/format-response')
const Log = require('../../helpers/log')

const db = admin.firestore()
const log = new Log(config.slack.development, 'Zone management - Grabber')

module.exports = functions
  .region('europe-west1')
  .https
  .onRequest(async (request, response) => {
    try {
      const hurrierResponse = await axios.get(config.hurrier.url, { headers: config.hurrier.headers })

      // async db operations
      const operations = []

      // select collection
      const collectionRef = db.collection(config.firestore.zones)

      // create timestamp for validity check
      const timestamp = new Date()

      // save docs
      hurrierResponse.data.data.forEach((item) => {
        const doc = {
          id: item.id,
          name: item.attributes.name,
          delay: item.attributes.delay,
          timestamp,
        }

        // write data to DB
        operations.push(collectionRef.doc(`${item.id}`).set(doc))
      })

      Promise.all(operations)
        .then(() => {
          console.info(hurrierResponse.data.data.map(item => ({ id: item.id, delay: item.attributes.delay })))
          response.status(200).send(formatResponse(200))
        })
        .catch((error) => {
          log.error(error)
          response.status(400).send(formatResponse(400, error))
        })
    } catch (error) {
      log.error(error)
      response.status(400).send(formatResponse(400, error))
    }
  })
