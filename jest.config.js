module.exports = {
  moduleFileExtensions: [
    'js',
    'json',
  ],
  collectCoverage: true,
  collectCoverageFrom: [
    '!/node_modules/',
    '<rootDir>/{functions,helpers}/**/*.js',
  ],
  coverageReporters: [
    'text-summary',
    'cobertura',
  ],
  coverageDirectory: '<rootDir>/shippable/codecoverage',
  reporters: [
    'default',
    'jest-junit',
  ],
  testURL: 'http://localhost/',
}
