const formatResponse = require('./index')

describe('Format response tests', () => {
  it('Formats response correctly', () => {
    const code = 200
    const data = 'sample data'

    const result = formatResponse(code, data)

    expect(result).toEqual({
      code: 200,
      data: 'sample data',
    })
  })
})
