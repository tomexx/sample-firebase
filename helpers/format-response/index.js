module.exports = function formatResponse(code, data) {
  const result = {}

  if (code) {
    result.code = code
  }
  if (data) {
    result.data = data
  }

  return result
}
