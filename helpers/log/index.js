const axios = require('axios')
const formatResponse = require('../format-response')

class Log {
  constructor(apiUrl, name) {
    this.apiUrl = apiUrl
    this.name = name
  }

  error(data) {
    console.error(formatResponse(400, data))
    return axios.put(this.apiUrl, {
      text: `${this.name} error:\n${JSON.stringify(data)}`,
    })
  }
}

module.exports = Log
